<?php

namespace Company\Payment\Tests;

use Company\Payment\CreditCard;
use PHPUnit\Framework\TestCase;

class CreditCardTest extends TestCase
{

    private $credit_card;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->credit_card = new CreditCard;
    }

    public function testValidNumber()
    {
        $this->assertTrue($this->credit_card->Set('4444333322221111'));
    }

    public function testInvalidNumberShouldReturError()
    {
        $this->assertEquals($this->credit_card::ERROR_INVALID_LENGTH, $this->credit_card->Set('3333555522221111'));
    }

    public function testValidNumberShouldSetAndGet()
    {
        $this->credit_card->Set('4444333322221111');
        $this->assertEquals('4444333322221111', $this->credit_card->Get());
    }
}
