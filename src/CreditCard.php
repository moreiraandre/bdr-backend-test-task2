<?php

/**
 * This class is responsible for storing a valid credit card number.
 */

namespace Company\Payment;

class CreditCard extends CreditCardSupport
{
    /**
     * @var string Number of CreditCard
     */
    private $_number;

    /**
     * Sets the card number if valid, otherwise returns the error message.
     *
     * @param null $number
     * @return bool|string
     */
    public function Set($number = null)
    {
        // clear current object values
        $this->_number = null;
        $this->setError(self::ERROR_NOT_SET);

        if ($number) {                             // anything passed?
            $isValid = $this->IsValid($number);
            if ($isValid) {                // yes, check
                $this->_number = $number;  // define number
                return $isValid;           // return true
            }
        }

        return $this->getError();              // if invalid return error message
    }

    /**
     * Retrieve the current card number. the number is returned unformatted suitable for use with submission to payment
     * and authorization gateways.
     *
     * @return string|null Card number
     */
    public function Get()
    {
        return $this->_number;
    }

}
