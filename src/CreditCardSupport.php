<?php

/**
 * This abstract class contains the features to assist in validating the credit card number. It has no public methods
 * except setters and getters. The methods here are defined in the final scope because the logic of this class is
 * official and cannot be overridden.
 */

namespace Company\Payment;

abstract class CreditCardSupport
{
    // MESSAGE VALIDATION ERROS
    const ERROR_INVALID_CHAR = 'ERROR_INVALID_CHAR';
    const ERROR_INVALID_LENGTH = 'ERROR_INVALID_LENGTH';
    const ERROR_NOT_SET = 'ERROR_NOT_SET';

    /**
     * @var string Error message
     */
    private $_error;

    /**
     * Set default to clear error.
     *
     * @return $this
     */
    private function clearError()
    {
        $this->_error = null;
        return $this;
    }

    /**
     * @param $error
     * @return null|string
     */
    final public function setError($error) {
        return $this->_error = $error;
    }

    /**
     * @return string
     */
    final public function getError() {
        return $this->_error;
    }

    /**
     * Valida se o tamanho do número corresponde a sua categoria
     *
     * @param $length
     * @param $category
     * @return bool
     */
    final protected function _check_length($length, $category)
    {
        switch (intval($category)) {
            case 0:
                return (($length == 13) || ($length == 16));
                break;
            case 1:
                return (($length == 16) || ($length == 18) || ($length == 19));
                break;
            case 2:
                return ($length == 16);
                break;
            case 3:
                return ($length == 15);
                break;
            case 4:
                return ($length == 14);
                break;
            default:
                return 1;
        }
    }

    /**
     * Checking that all characters are valid
     *
     * @param $number string CreditCard number
     * @return string Error message or CreditCard number
     */
    final protected function verifyInvalidChars($number)
    {
        $lengthNumber = strlen($number);
        $value = '';                                               // init copy buffer
        for ($i = 0; $i < $lengthNumber; $i++) {                   // check input
            $c = $number[$i];                                      // grab a character

            if (ctype_digit($c))                                   // is a digit?
                $value .= $c;                                      // yes, save it
            elseif (!ctype_space($c) && !ctype_punct($c))          // otherwise return error
                return $this->setError(self::ERROR_INVALID_CHAR);
        }

        return $value;
    }

    /**
     * Identifies the card for the number length.
     *
     * @param $number
     * @return int|null
     */
    final protected function lengthCategory($number)
    {
        /*
         * Visa = 4XXX - XXXX - XXXX - XXXX
         * MasterCard = 5[1-5]XX - XXXX - XXXX - XXXX
         * Discover = 6011 - XXXX - XXXX - XXXX
         * Amex = 3[4,7]X - XXXX - XXXX - XXXX
         * Diners = 3[0,6,8] - XXXX - XXXX - XXXX
         * Any Bankcard = 5610 - XXXX - XXXX - XXXX
         * JCB =  [3088|3096|3112|3158|3337|3528] - XXXX - XXXX - XXXX
         * Enroute = [2014|2149] - XXXX - XXXX - XXX
         * Switch = [4903|4911|4936|5641|6333|6759|6334|6767] - XXXX - XXXX - XXXX
         */
        $lencat = null;
        switch (intval($number[0])) {
            case 4:
            case 5:
                $lencat = 2;
                break;
            case 3:
                $lencat = 4;
                break;
            case 2:
                $lencat = 3;
                break;
        }

        return $lencat;
    }

    /**
     * Validate credit card number.
     *
     * @param null $number
     * @return bool|string|null
     */
    final protected function IsValid($number = null)
    {
        if ($number) { // new validation?
            $verifyInvalidChars = $this->verifyInvalidChars($number);
            if ($verifyInvalidChars == self::ERROR_INVALID_CHAR)
                return null;

            $lencat = $this->lengthCategory($number);
            if (!$this->_check_length(strlen($number), $lencat))
                $this->setError(self::ERROR_INVALID_LENGTH);
            else {
                $this->clearError();
                return true;
            }
        } else
            $this->setError(self::ERROR_INVALID_CHAR);

        return null;
    }
}
